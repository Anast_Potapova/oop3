package com.company.suppliers;

import com.company.PropertiesManager;
import com.company.details.BodyDetail;
import com.company.stores.BodyStore;

import java.io.IOException;

public class BodySupplier extends Supplier {

    public BodySupplier(ThreadGroup group, String name, BodyStore store) {
        super(group, name, store);
        try {
            workTime = PropertiesManager.getValue("bodyMakingTime");
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                sleep(workTime);
                ((BodyStore) store).put(new BodyDetail());
            } catch (InterruptedException e) {
                break;
            }
        }
        System.err.println(getName() + " finished.");
    }
}
