package com.company;

import com.company.details.CarDetail;
import com.company.stores.AccessoryStore;
import com.company.stores.BodyStore;
import com.company.stores.CarStore;
import com.company.stores.EngineStore;
import com.company.threadpool.Task;

import java.io.IOException;

public class Worker implements Task {

    private static int count = 0;
    private static int workingTime;

    static {
        try {
            workingTime = PropertiesManager.getValue("carMakingTime");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private final BodyStore bodyStore;
    private final EngineStore engineStore;
    private final AccessoryStore accessoryStore;
    private final CarStore carStore;
    private final int number;

    public Worker(BodyStore bodyStore, EngineStore engineStore, AccessoryStore accessoryStore, CarStore carStore) {
        this.bodyStore = bodyStore;
        this.engineStore = engineStore;
        this.accessoryStore = accessoryStore;
        this.carStore = carStore;
        count++;
        number = count;
    }

    public static void changeWorkTime(int newTime) {
        workingTime = newTime;
    }

    @Override
    public String getName() {
        return "Worker #" + number;
    }

    @Override
    public void performWork() throws InterruptedException {
        CarDetail car = new CarDetail(bodyStore.get(), engineStore.get(), accessoryStore.get());
        Thread.sleep(workingTime);
        carStore.put(car);
    }

}
