package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    public static int getValue(String name) throws IOException {
        Properties properties = new Properties();
        //InputStream in = PropertiesManager.class.getResourceAsStream("TRYtoDOit/src/value.properties");
        InputStream in = PropertiesManager.class.getClassLoader().getResourceAsStream("value.properties");
        properties.load(in);
        //properties.load(PropertiesManager.class.getResourceAsStream("src/com.company/value.properties"));
        return Integer.parseInt(properties.getProperty(name));
    }
}