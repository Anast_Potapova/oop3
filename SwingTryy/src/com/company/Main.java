package com.company;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;


public class Main {
    public static void main(String[] args) {
        new GUI();
    }

    public static class GUI extends JFrame {

        private BoundedRangeModel model;

        public GUI(){
            super("Cars factory");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            JLabel labelBody = new JLabel("Making body takes");
            JLabel labelEngine = new JLabel("Making engine takes");
            JLabel labelAccessory = new JLabel("Making accessory takes");
            JLabel labelCars = new JLabel("Making car takes");
            JLabel labelDealer = new JLabel("Dealer want a car each");


            JSlider sliderBody = new JSlider(0, 1000);
            JSlider sliderEngine = new JSlider(0, 1000);
            JSlider sliderAccessory = new JSlider(0, 1000);
            JSlider sliderCars = new JSlider(0, 1000);
            JSlider sliderDealer = new JSlider(0, 1000);

            JLabel labelB = new JLabel(getNumber(sliderBody.getValue()));
            JLabel labelE = new JLabel(getNumber(sliderEngine.getValue()));
            JLabel labelA = new JLabel(getNumber(sliderAccessory.getValue()));
            JLabel labelC = new JLabel(getNumber(sliderCars.getValue()));
            JLabel labelD = new JLabel(getNumber(sliderDealer.getValue()));

            model = new DefaultBoundedRangeModel(0, 0, 0, 10);

            JProgressBar progressBarB = new JProgressBar(model);
            JProgressBar progressBarE = new JProgressBar(model);
            JProgressBar progressBarA = new JProgressBar(model);
            JProgressBar progressBarC = new JProgressBar(model);


            sliderBody.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    int value = ((JSlider) e.getSource()).getValue();
                    labelB.setText(getNumber(value));
                }
            });

            sliderEngine.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    int value = ((JSlider) e.getSource()).getValue();
                    labelE.setText(getNumber(value));
                }
            });

            sliderAccessory.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    int value = ((JSlider) e.getSource()).getValue();
                    labelA.setText(getNumber(value));
                }
            });

            sliderCars.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    int value = ((JSlider) e.getSource()).getValue();
                    labelC.setText(getNumber(value));
                }
            });

            sliderDealer.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    int value = ((JSlider) e.getSource()).getValue();
                    labelD.setText(getNumber(value));
                }
            });



            Container container = this.getContentPane();
            container.setLayout(new GridLayout(5,4,2,2));

            container.add(labelBody);
            container.add(sliderBody);
            container.add(labelB);
            container.add(progressBarB);

            container.add(labelEngine);
            container.add(sliderEngine);
            container.add(labelE);
            container.add(progressBarE);

            container.add(labelAccessory);
            container.add(sliderAccessory);
            container.add(labelA);
            container.add(progressBarA);

            container.add(labelCars);
            container.add(sliderCars);
            container.add(labelC);
            container.add(progressBarC);

            container.add(labelDealer);
            container.add(sliderDealer);
            container.add(labelD);



            setSize(800, 400);
            setVisible(true);
            new StoreProgressListener().start();
        }

        // Поток эмуляции некоторого процесса
        class StoreProgressListener extends Thread {
            public void run() {
                // Проверка завершения процесса
                while ( model.getValue() < model.getMaximum() ) {
                    try {
                        // Увеличение текущего значение
                        model.setValue(model.getValue() + 1);
                        // Случайная временная задержка
                        sleep((int)(Math.random() * 1000));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }



        private String getNumber(int value) {
            return (int) value + " times";
        }
    }

}
