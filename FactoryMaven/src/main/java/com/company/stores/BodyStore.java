package com.company.stores;

import com.company.details.BodyDetail;

public class BodyStore extends Store {

    public BodyStore(int size) {
        super(size);
    }

    @Override
    public synchronized BodyDetail get() {
        return (BodyDetail) super.get();
    }

    public synchronized void put(BodyDetail detail) {
        System.err.println("BodyStore: " + details.size() + "/" + limit);
        try {
            while (details.size() >= limit) {
                // System.err.println("Body store is empty.");
                wait();
            }
            details.push(detail);
            notify();
            synchronized (sizeMonitor) {
                sizeMonitor.setSize(sizeMonitor.getSize() + 1);
                sizeMonitor.notify();
            }
        } catch (InterruptedException e) {
            System.err.println("Body store interrupted.");
        }
    }
}
