package com.company;

import com.company.stores.*;
import com.company.suppliers.AccessorySupplier;
import com.company.suppliers.BodySupplier;
import com.company.suppliers.EngineSupplier;
import com.company.threadpool.ThreadPool;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.IOException;

public class Controller {

    private static ThreadPool workersPool;
    private static EngineStore engineStore;
    private static AccessoryStore accessoryStore;
    private static BodyStore bodyStore;
    private static CarStore carStore;

    private static ThreadGroup bodySuppliers;
    private static ThreadGroup engineSuppliers;
    private static ThreadGroup accessorySuppliers;
    private static ThreadGroup dealers;

    private static StoreProgressListener bodyListener;
    private static StoreProgressListener engineListener;
    private static StoreProgressListener accessoryListener;
    private static StoreProgressListener carListener;


    public void initialize() {
        initModel();

        GUI.sliderBody.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                GUI.labelB.setText(getNumber(value));
                try {
                    BodySupplier[] bodySuppliersArray = new BodySupplier[PropertiesManager.getValue("bodySuppliers")];
                    bodySuppliers.enumerate(bodySuppliersArray);
                    for (BodySupplier bodySupplier : bodySuppliersArray) {
                        System.err.println("Try to set new time");
                        bodySupplier.changeWorkTime(value);
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });

        GUI.sliderEngine.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                GUI.labelE.setText(getNumber(value));
                try {
                    EngineSupplier[] engineSuppliersArray = new EngineSupplier[PropertiesManager.getValue("bodySuppliers")];
                    engineSuppliers.enumerate(engineSuppliersArray);
                    for (EngineSupplier bodySupplier : engineSuppliersArray) {
                        System.err.println("Try to set new time");
                        bodySupplier.changeWorkTime(value);
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });


        GUI.sliderAccessory.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                GUI.labelA.setText(getNumber(value));
                try {
                    AccessorySupplier[] accessorySuppliersArray = new AccessorySupplier[PropertiesManager.getValue("accessorySuppliers")];
                    accessorySuppliers.enumerate(accessorySuppliersArray);
                    for (AccessorySupplier accessorySupplier : accessorySuppliersArray) {
                        System.err.println("Try to set new time");
                        accessorySupplier.changeWorkTime(value);
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });

        GUI.sliderCars.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                GUI.labelC.setText(getNumber(value));
                Worker.changeWorkTime(value);
            }
        });

        GUI.sliderDealer.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                GUI.labelD.setText(getNumber(value));
                try {
                    Dealer[] dealersArray = new Dealer[PropertiesManager.getValue("dealers")];
                    dealers.enumerate(dealersArray);
                    for (Dealer dealer : dealersArray) {
                        System.err.println("Try to set new time");
                        dealer.changeDealingTime(value);
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });

        try {
            GUI.sliderBody.setValue(PropertiesManager.getValue("bodyMakingTime"));
            GUI.sliderEngine.setValue(PropertiesManager.getValue("engineMakingTime"));
            GUI.sliderAccessory.setValue(PropertiesManager.getValue("accessoryMakingTime"));
            GUI.sliderCars.setValue(PropertiesManager.getValue("carMakingTime"));
            GUI.sliderDealer.setValue(PropertiesManager.getValue("dealerTime"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        bodyListener = new StoreProgressListener(bodyStore, GUI.progressBarB);
        bodyListener.start();
        engineListener = new StoreProgressListener(engineStore, GUI.progressBarE);
        engineListener.start();
        accessoryListener = new StoreProgressListener(accessoryStore, GUI.progressBarA);
        accessoryListener.start();
        carListener = new StoreProgressListener(carStore, GUI.progressBarC);
        carListener.start();


    }


    private String getNumber(int value) {
        return (int) value + " times";
    }


    private void initModel() {
        try {
            workersPool = new ThreadPool(PropertiesManager.getValue("workers"));

            engineStore = new EngineStore(PropertiesManager.getValue("engineStoreSize"));
            accessoryStore = new AccessoryStore(PropertiesManager.getValue("accessoryStoreSize"));
            bodyStore = new BodyStore(PropertiesManager.getValue("bodyStoreSize"));
            carStore = new CarStore(PropertiesManager.getValue("carsStoreSize"));

            bodySuppliers = new ThreadGroup("bodySuppliers");
            engineSuppliers = new ThreadGroup("engineSuppliers");
            accessorySuppliers = new ThreadGroup("accessorySuppliers");
            dealers = new ThreadGroup("dealers");

            for (int i = 0; i < PropertiesManager.getValue("bodySuppliers"); i++) {
                new BodySupplier(bodySuppliers, "Body supplier #" + i, bodyStore).start();
            }
            for (int i = 0; i < PropertiesManager.getValue("engineSuppliers"); i++) {
                new EngineSupplier(engineSuppliers, "Engine supplier #" + i, engineStore).start();
            }
            for (int i = 0; i < PropertiesManager.getValue("accessorySuppliers"); i++) {
                new AccessorySupplier(accessorySuppliers, "Accessory supplier #" + i, accessoryStore).start();
            }
            for (int i = 0; i < PropertiesManager.getValue("dealers"); i++) {
                new Dealer(dealers, "Dealer #" + i, carStore).start();
            }

            CarStoreController controller = new CarStoreController(carStore, workersPool, engineStore, accessoryStore, bodyStore);
            controller.start();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void shutdown() {
        bodySuppliers.interrupt();
        engineSuppliers.interrupt();
        accessorySuppliers.interrupt();
        dealers.interrupt();

        bodyListener.interrupt();
        engineListener.interrupt();
        accessoryListener.interrupt();
        carListener.interrupt();

        workersPool.interrupt();
    }

}
