package ru.nsu.calc;

import java.io.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
Недочёты:

//slf4j
Логирование изменено. Теперь используется библиотека slf4j.

//properties
//reflection
Загрузка классов команд происходит из конфигурационного файла.

Рефлексия — это механизм исследования данных о программе во время её выполнения.
Рефлексия позволяет исследовать информацию о полях, методах и конструкторах классов.
Сам же механизм рефлексии позволяет обрабатывать типы, отсутствующие при компиляции,
но появившиеся во время выполнения программы
newInstance() позволяет создать экземпляр класса (причем имя класса неизвестно до момента выполнения программы)

//delete s
Передача строки s из интерфейса не убрана, тк она нужна для реализации классов define и comment.
Из остальных классов строка s убрана, как ненужная. Теперь проверка на кол-во
аргументов происходит в методе main класса ru.nsu.calc.Main в ужасном switch`е.

//getResourceAsStream
Поиск файла по абсолютному пути

//Test
все тесты внесены в отдельный класс

*/


public class Main {


    private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) throws FileNotFoundException {

        Scanner scanner;
        String s;
        Stack<Double> stack = new Stack<>();

        if (args.length > 0) {
            FileInputStream fin = new FileInputStream(args[1]);
            scanner = new Scanner(fin);
        } else {
            scanner = new Scanner(System.in);
        }

        Map<String, Double> varMap = new HashMap<>();
        Factory factory = Factory.getInst();

        while (scanner.hasNextLine()) {
            s = scanner.nextLine();
            LOGGER.info(s + "\n");
            String[] str = s.split(" ");
            Command kom = factory.getCommandByName(str[0]);
            if (kom != null) {


                switch (str[0]) {
                    case "push":
                        if (str.length == 2) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input1!");
                        }
                        break;
                    case "define":
                        if (str.length == 3) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input2!");
                        }
                        break;
                    case "comment":
                        if (str.length > 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input3!");
                        }
                        break;
                    case "pop":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input4!");
                        }
                        break;
                    case "print":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input5!");
                        }
                        break;
                    case "plus":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input6!");
                        }
                        break;
                    case "minus":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input7!");
                        }
                        break;
                    case "multiply":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input8!");
                        }
                        break;
                    case "divide":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input9!");
                        }
                        break;
                    case "sqrt":
                        if (str.length == 1) {
                            tryKom(kom, stack, varMap, str);
                        } else {
                            System.out.println("Bad input10!");
                        }
                        break;

                }

            } else {
                System.out.println("There is no command");
            }


        }


    }

    public static void tryKom(Command kom, Stack stack, Map varMap, String[] str) {
        try {
            kom.exec(stack, varMap, str);
        } catch (NumberFormatException e) {
            System.out.println("I do not know that");
        }
    }


}