package com.company;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class GUI extends JFrame {

    public static JSlider sliderBody;
    public static JSlider sliderEngine;
    public static JSlider sliderAccessory;
    public static JSlider sliderCars;
    public static JSlider sliderDealer;

    public static JLabel labelB;
    public static JLabel labelE;
    public static JLabel labelA;
    public static JLabel labelC;
    public static JLabel labelD;

    public static JProgressBar progressBarB;
    public static JProgressBar progressBarE;
    public static JProgressBar progressBarA;
    public static JProgressBar progressBarC;

    public static JLabel total;

    public GUI() throws IOException {
        super("Cars factory");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel labelBody = new JLabel("Making body takes");
        JLabel labelEngine = new JLabel("Making engine takes");
        JLabel labelAccessory = new JLabel("Making accessory takes");
        JLabel labelCars = new JLabel("Making car takes");
        JLabel labelDealer = new JLabel("Dealer want a car each");

        sliderBody = new JSlider(0, 1000);
        sliderEngine = new JSlider(0, 1000);
        sliderAccessory = new JSlider(0, 1000);
        sliderCars = new JSlider(0, 1000);
        sliderDealer = new JSlider(0, 1000);

        labelB = new JLabel(getNumber(sliderBody.getValue()));
        labelE = new JLabel(getNumber(sliderEngine.getValue()));
        labelA = new JLabel(getNumber(sliderAccessory.getValue()));
        labelC = new JLabel(getNumber(sliderCars.getValue()));
        labelD = new JLabel(getNumber(sliderDealer.getValue()));

        progressBarB = new JProgressBar(0, PropertiesManager.getValue("bodyStoreSize"));
        progressBarE = new JProgressBar(0, PropertiesManager.getValue("engineStoreSize"));
        progressBarA = new JProgressBar(0, PropertiesManager.getValue("accessoryStoreSize"));
        progressBarC = new JProgressBar(0, PropertiesManager.getValue("carsStoreSize"));

        progressBarB.setStringPainted(true);
        progressBarE.setStringPainted(true);
        progressBarA.setStringPainted(true);
        progressBarC.setStringPainted(true);

        total = new JLabel("Total:0");





        Container container = this.getContentPane();
        container.setLayout(new GridLayout(5, 4, 2, 2));

        container.add(labelBody);
        container.add(sliderBody);
        container.add(labelB);
        container.add(progressBarB);

        container.add(labelEngine);
        container.add(sliderEngine);
        container.add(labelE);
        container.add(progressBarE);

        container.add(labelAccessory);
        container.add(sliderAccessory);
        container.add(labelA);
        container.add(progressBarA);

        container.add(labelCars);
        container.add(sliderCars);
        container.add(labelC);
        container.add(progressBarC);

        container.add(labelDealer);
        container.add(sliderDealer);
        container.add(labelD);

        container.add(total);



        setSize(1000, 500);
        setVisible(true);
    }


    private String getNumber(int value) {
        return value + " times";
    }
}