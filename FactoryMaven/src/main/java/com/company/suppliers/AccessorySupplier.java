package com.company.suppliers;

import com.company.PropertiesManager;
import com.company.details.AccessoryDetail;
import com.company.stores.AccessoryStore;

import java.io.IOException;

public class AccessorySupplier extends Supplier {

    public AccessorySupplier(ThreadGroup group, String name, AccessoryStore store) {

        super(group, name, store);

        try {
            workTime = PropertiesManager.getValue("accessoryMakingTime");
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                sleep(workTime);
                ((AccessoryStore) store).put(new AccessoryDetail());
            } catch (InterruptedException e) {
                break;
            }
        }
        System.err.println(getName() + " finished.");
    }
}