package com.company;

import com.company.stores.Store;

import javax.swing.*;

public class StoreProgressListener extends Thread {

    private final SizeMonitor sizeMonitor;
    private final JProgressBar progressBar;

    public StoreProgressListener(Store store, JProgressBar progressBar) {
        this.sizeMonitor = store.sizeMonitor;
        this.progressBar = progressBar;
    }

    @Override
    public void run() {
        synchronized (sizeMonitor) {
            try {
                while (!isInterrupted()) {
                    sizeMonitor.wait();
                    //System.err.println("Monitor response");
                    progressBar.setValue(sizeMonitor.getSize());
                }
            } catch (InterruptedException e) {
                System.err.println("Progress listener interrupted.");
            }
        }
    }
}