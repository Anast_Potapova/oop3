package ru.nsu.calc;

import java.util.Map;
import java.util.Stack;

public class Define implements Command{
    @Override
    public void exec(Stack<Double> stack, Map<String, Double> define, String[] s) {
        define.put(s[1], Double.valueOf(s[2]));
    }
}
