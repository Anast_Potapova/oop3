package com.company;

import javax.swing.*;

public class ProgressBar extends JFrame
{
    // Общая модель
    private BoundedRangeModel model;
    public ProgressBar() {
        super("Пример использования JProgressBar");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Создание стандартной модели
        model = new DefaultBoundedRangeModel(5, 0, 0, 100);

        // Горизонтальный индикатор
        JProgressBar pbHorizontal = new JProgressBar(model);
        //pbHorizontal.setStringPainted(true);//отрисовка процентов


        // Размещение индикаторов в окне
        JPanel contents = new JPanel();
        contents.add(pbHorizontal);

        // Вывод окна на экран
        setContentPane(contents);
        setSize(360, 220);
        setVisible(true);
        // Старт "процесса"
        new StoreProgressListener().start();
    }
    // Поток эмуляции некоторого процесса
    class StoreProgressListener extends Thread {
        public void run() {
            // Проверка завершения процесса
            while ( model.getValue() < model.getMaximum() ) {
                try {
                    // Увеличение текущего значение
                    model.setValue(model.getValue() + 1);
                    // Случайная временная задержка
                    sleep((int)(Math.random() * 1000));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) {
        new ProgressBar();
    }
}
