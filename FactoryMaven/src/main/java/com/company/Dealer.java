package com.company;

import com.company.details.CarDetail;
import com.company.stores.CarStore;

import java.io.IOException;
import java.util.logging.Logger;

public class Dealer extends Thread {

    private static final Logger logger = Logger.getLogger(Dealer.class.getName());
    private static int count;
    private final CarStore carStore;
    private final int number;
    private int dealingTime;

    private int totalCount = 0;

    public Dealer(ThreadGroup group, String name, CarStore carsStore) {
        super(group, name);
        this.carStore = carsStore;
        try {
            dealingTime = PropertiesManager.getValue("dealerTime");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        count++;
        number = count;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                totalCount++;
                sleep(dealingTime);
                CarDetail car = carStore.get();
                System.err.println("Dealer #" + number + " get: " + car.getUID());
                GUI.total.setText("Total:" + totalCount);

                logger.info("Dealer #" + number + " car: " + car.getUID()
                        + " body: " + car.getBody().getUID()
                        + " engine: " + car.getEngine().getUID()
                        + " accessory: " + car.getAccessory().getUID());

            } catch (InterruptedException e) {
                break;
            }
        }
        System.err.println(getName() + " finished.");
    }

    public synchronized void changeDealingTime(int newTime) {
        System.out.println("Dealer time changed : " + newTime);
        dealingTime = newTime;
    }

}

