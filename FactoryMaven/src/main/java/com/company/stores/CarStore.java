package com.company.stores;

import com.company.details.CarDetail;

public class CarStore extends Store {

    public final Object controllerMonitor = new Object();

    public CarStore(int size) {
        super(size);
    }

    @Override
    public synchronized CarDetail get() {
        // System.err.println("Get");
        synchronized (controllerMonitor) {
            // System.err.println("Get contr monitor in store.");
            controllerMonitor.notify();
            try {
                controllerMonitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return (CarDetail) super.get();
    }

    public synchronized void put(CarDetail detail) {
        System.err.println("Car put.");
        if (details.size() < limit) {
            details.push(detail);
            // System.err.println("Add car : " + detail.getUID());
            System.err.println("Cars store: " + details.size() + "/" + limit);
            notifyAll();
            synchronized (sizeMonitor) {
                sizeMonitor.setSize(sizeMonitor.getSize() + 1);
                sizeMonitor.notify();
            }
        } else System.err.println("Cars store is full!");
    }

}
