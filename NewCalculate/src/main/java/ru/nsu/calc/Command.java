package ru.nsu.calc;

import java.util.Map;
import java.util.Stack;

public interface Command {
    void exec(Stack<Double> stack, Map<String, Double> define, String[] s);
}
