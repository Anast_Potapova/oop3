package com.company.stores;

import com.company.details.AccessoryDetail;

public class AccessoryStore extends Store {

    public AccessoryStore(int size) {
        super(size);
    }

    @Override
    public synchronized AccessoryDetail get() {
        return (AccessoryDetail) super.get();
    }

    public synchronized void put(AccessoryDetail detail) {
        System.err.println("AccessoryStore: " + details.size() + "/" + limit);
        try {
            while (details.size() >= limit) {
                // System.err.println("Accessory store is empty.");
                wait();
            }
            details.push(detail);
            notify();
            synchronized (sizeMonitor) {
                sizeMonitor.setSize(sizeMonitor.getSize() + 1);
                sizeMonitor.notify();
            }
        } catch (InterruptedException e) {
            System.err.println("Accessory store interrupted.");
        }
    }
}
