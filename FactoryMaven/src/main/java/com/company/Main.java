package com.company;

import java.io.IOException;

public class Main {


    public static void main(String[] args) throws IOException {
        new GUI();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Controller().initialize();
    }


}
