package ru.nsu.calc;

import java.util.Map;
import java.util.Stack;

public class Push implements Command{
    @Override
    public void exec(Stack<Double> stack, Map<String, Double> define, String[] s) {

        /*
        The java.util.HashMap.containsKey() method is used to
        check whether a particular key is being mapped into
        the HashMap or not. It takes the key element as a
        parameter and returns True if that element is mapped
        in the map.
        Нужен для define'а
        */
        if (define.containsKey(s[1])) {
            stack.push(define.get(s[1]));
        } else {
            stack.push(Double.valueOf(s[1]));
        }

    }
}
