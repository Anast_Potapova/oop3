package ru.nsu.calc;

import java.util.Map;
import java.util.Stack;

public class Comment implements Command{
    @Override
    public void exec(Stack<Double> stack, Map<String, Double> define, String[] s) {
        for (String word : s) {
            System.out.print(word + " ");
        }
        System.out.println();
    }
}
