import ru.nsu.calc.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test {

    private Stack<Double> stack = new java.util.Stack<>();
    private Map<String, Double> map = new HashMap<>();

    @org.junit.jupiter.api.Test
    public void CommentTest() {
        Command s = new Comment();
        String[] str = {"comment", "lalala"};
        s.exec(stack, map, str);
        assertEquals("lalala", str[1]);
    }

    @org.junit.jupiter.api.Test
    void DefineTest() {
        Command s = new Define();
        Command k = new Push();
        s.exec(stack, map, new String[]{"define", "a", "4."});
        k.exec(stack, map, new String[]{"push", "a"});
        assertEquals(4., stack.peek());
    }

    @org.junit.jupiter.api.Test
    void DivideTest() {
        Command s = new Divide();
        stack.push(30.);
        stack.push(5.);
        s.exec(stack, map, new String[0]);
        assertEquals(6., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void MinusTest() {
        Command s = new Minus();
        stack.push(30.);
        stack.push(5.);
        s.exec(stack, map, new String[0]);
        assertEquals(25., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void MultiplyTest() {
        Command s = new Multiply();
        stack.push(30.);
        stack.push(5.);
        s.exec(stack, map, new String[0]);
        assertEquals(150., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void PlusTest() {
        Command s = new Plus();
        stack.push(25.);
        stack.push(5.);
        s.exec(stack, map, new String[0]);
        assertEquals(30., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void PopTest() {
        Command s = new Pop();
        stack.push(5.);
        stack.push(6.);
        s.exec(stack, map, new String[]{"pop"});
        assertEquals(5., stack.peek());
    }

    @org.junit.jupiter.api.Test
    void PrintTest() {
        Command s = new Print();
        stack.push(10.);
        s.exec(stack, map, new String[]{"print"});
        assertEquals(10., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void PushTest() {
        Command s = new Push();
        s.exec(stack, map, new String[]{"push", "7"});
        assertEquals(7., stack.peek());
        assertEquals(1, stack.size());
    }

    @org.junit.jupiter.api.Test
    void SqrtTest() {
        Command s = new Sqrt();
        stack.push(25.);
        s.exec(stack, map, new String[0]);
        assertEquals(5., stack.peek());
        assertEquals(1, stack.size());
    }
}
