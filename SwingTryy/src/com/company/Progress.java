package com.company;

import javax.swing.*;

public class Progress extends JFrame
{
    // Общая модель
    private BoundedRangeModel model;
    public Progress() {
        super("Пример использования JProgressBar");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Создание стандартной модели
        model = new DefaultBoundedRangeModel(0, 0, 0, 100);


        // Горизонтальный индикатор
        JProgressBar pbHorizontal = new JProgressBar(model);
        pbHorizontal.setStringPainted(true);

        // Настройка параметрой UI-представителя
        UIManager.put("ProgressBar.cellSpacing", 5);
        UIManager.put("ProgressBar.cellLength", 15);
        // Создание индикатора
        JProgressBar pbUIManager = new JProgressBar(model);



        // Размещение индикаторов в окне
        JPanel contents = new JPanel();
        contents.add(pbHorizontal);
        //contents.add(pbUIManager );

        // Вывод окна на экран
        setContentPane(contents);
        setSize(360, 220);
        setVisible(true);
        // Старт "процесса"
        new ThreadProcess().start();
    }
    // Поток эмуляции некоторого процесса
    class ThreadProcess extends Thread {
        public void run() {
            // Проверка завершения процесса
            while ( model.getValue() < model.getMaximum() ) {
                try {
                    // Увеличение текущего значение
                    model.setValue(model.getValue() + 1);
                    // Случайная временная задержка
                    sleep((int)(Math.random() * 1000));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) {
        new Progress();
    }
}
