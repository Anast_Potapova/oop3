package com.company.stores;

import com.company.details.EngineDetail;

public class EngineStore extends Store {

    public EngineStore(int size) {
        super(size);
    }

    @Override
    public synchronized EngineDetail get() {
        return (EngineDetail) super.get();
    }

    public synchronized void put(EngineDetail detail) {
        System.err.println("EngineStore: " + details.size() + "/" + limit);
        try {
            while (details.size() >= limit) {
                // System.err.println("Engine store is empty.");
                wait();
            }
            details.push(detail);
            notify();
            synchronized (sizeMonitor) {
                sizeMonitor.setSize(sizeMonitor.getSize() + 1);
                sizeMonitor.notify();
            }
        } catch (InterruptedException e) {
            System.err.println("Engine store interrupted.");
        }
    }

}

