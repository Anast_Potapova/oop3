package ru.nsu.calc;

import java.util.Map;
import java.util.Stack;

public class Pop implements Command{
    @Override
    public void exec(Stack<Double> stack, Map<String, Double> define, String[] s) {
        if (stack.size() == 0) {
            System.out.println("Stack is empty!");
        } else {
            stack.pop();
        }
    }
}
