package com.company.suppliers;

import com.company.PropertiesManager;
import com.company.details.EngineDetail;
import com.company.stores.EngineStore;

import java.io.IOException;

public class EngineSupplier extends Supplier {

    public EngineSupplier(ThreadGroup group, String name, EngineStore store) {
        super(group, name, store);
        try {
            workTime = PropertiesManager.getValue("engineMakingTime");
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }


    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                sleep(workTime);
                ((EngineStore) store).put(new EngineDetail());
            } catch (InterruptedException e) {
                break;
            }
        }
        System.err.println(getName() + " finished.");
    }

}
