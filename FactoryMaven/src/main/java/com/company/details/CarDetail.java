package com.company.details;

public class CarDetail extends Detail {

    private static long count = 0;

    private BodyDetail body;
    private EngineDetail engine;
    private AccessoryDetail accessory;

    public CarDetail(BodyDetail body, EngineDetail engine, AccessoryDetail accessory) {

        this.body = body;
        this.engine = engine;
        this.accessory = accessory;

        UID = count;
        count++;
    }

    public BodyDetail getBody() {
        return body;
    }

    public EngineDetail getEngine() {
        return engine;
    }

    public AccessoryDetail getAccessory() {
        return accessory;
    }
}
