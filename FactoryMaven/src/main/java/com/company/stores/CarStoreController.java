package com.company.stores;

import com.company.Worker;
import com.company.threadpool.ThreadPool;


public class CarStoreController extends Thread {

    private final CarStore carStore;
    private final ThreadPool workersPool;
    private final EngineStore engineStore;
    private final AccessoryStore accessoryStore;
    private final BodyStore bodyStore;

    public CarStoreController(CarStore carStore, ThreadPool workersPool, EngineStore engineStore, AccessoryStore accessoryStore, BodyStore bodyStore) {
        this.carStore = carStore;
        this.workersPool = workersPool;
        this.engineStore = engineStore;
        this.accessoryStore = accessoryStore;
        this.bodyStore = bodyStore;
    }

    @Override
    public void run() {

        while (true) {
            try {
                // System.err.println("Controller get!");
                synchronized (carStore.controllerMonitor) {
                    // System.err.println("Get monitor in controller");
                    carStore.controllerMonitor.wait();
                    workersPool.addTask(new Worker(bodyStore, engineStore, accessoryStore, carStore));

                    // System.err.println("Notified!");
                    carStore.controllerMonitor.notify();
                }
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}

