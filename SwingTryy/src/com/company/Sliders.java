package com.company;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.BorderLayout;

public class Sliders extends JFrame {
    private JLabel label;

    public Sliders() {
        super("Пример использования ползунков JSlider");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JSlider slider1 = new JSlider(0, 1000);
        label = new JLabel(getNumber(slider1.getValue()));

        slider1.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                // меняем надпись
                int value = ((JSlider) e.getSource()).getValue();
                label.setText(getNumber(value));
            }
        });

        JPanel contents = new JPanel();
        contents.add(slider1);
        getContentPane().add(contents);
        getContentPane().add(label, BorderLayout.SOUTH);

        setSize(500, 300);
        setVisible(true);
    }

    private String getNumber(int value) {
        return "Размер : " + (int) value;
    }

    public static void main(String[] args) {
        new Sliders();
    }
}

