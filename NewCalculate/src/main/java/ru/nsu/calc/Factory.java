package ru.nsu.calc;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Factory {

    private final Map<String, Command> cmdMap = new HashMap<>();
    private static Factory inst = null;

    static {
        try {
            inst = new Factory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Factory() throws IOException {
        Properties properties = new Properties();



        //работа с абсолютным путем
        try(InputStream in = Factory.class.getResourceAsStream("/commands.properties")){
            properties.load(in);
        }catch (IOException e){
            System.out.println("You have error in reading commands.properties!");
        }


        for (String key : properties.stringPropertyNames()){
            properties.get(key);
            try{
                Class cl = Class.forName(properties.getProperty(key));
                cmdMap.put(key, (Command) cl.getDeclaredConstructor().newInstance());
            } catch (Exception e) {
                System.out.println("Can`t reading: " + key );
            }
        }
    }

    public Command getCommandByName (String commandName){
        return cmdMap.get(commandName);
    }

    public static Factory getInst(){
        return inst;
    }

}
